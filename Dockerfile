FROM alpine:latest
RUN apk add --no-cache nodejs npm
WORKDIR /app
COPY . /app
COPY package.json /app
RUN npm install
RUN npm install nodemon
COPY . /app
EXPOSE 3001
CMD ["npm", "start"]