const Location = require("../models/location");

const addLocation = async (locationObj) => {
    try {
        const checkLocations = await Location.find({ address: locationObj.address });

        if (checkLocations?.length !== 0) {
            return {
                status: false,
                payload: "Location should be unique",
            }
        }

        const location = new Location(locationObj);
        await location.save();
        
        return {
            status: true,
            payload: location,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getLocations = async () => {
    try {
        const location = await Location.find({});
        
        return {
            status: true,
            payload: location,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getLocation = async (locationId) => {
    try {
        const location = await Location.findOne({ _id: locationId, });
        
        return {
            status: true,
            payload: location,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const updateLocationStock = async (id, stockItem) => {
    try {
        const location = await Location.findOne({ _id: id });

        if (!location) {
            return {
                status: false,
                payload: "Location not found",
            };
        }

        if (location.stock.find(item => item.name === stockItem.name)) {
            location.stock = location.stock.map(item => {
                if (item.name === stockItem.name) {
                    if (stockItem.available) {
                        item.available = Math.max(item.available + stockItem.available, 0);
                    }
                    if (stockItem.pending) {
                        item.pending = Math.max(item.pending + stockItem.pending, 0);
                    }
                }
    
                return item;
            });
        }
        else {
            location.stock.push(stockItem);
        }

        await location.save();
        
        return {
            status: true,
            payload: location,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    getLocations,
    getLocation,
    addLocation,
    updateLocationStock,
}
