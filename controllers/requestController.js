const Request = require("../models/request");
const {
    updateLocationStock,
} = require("./locationController")

const addRequest = async (requestObj) => {
    try {
        const request = new Request(requestObj);
        await request.save();
        
        return {
            status: true,
            payload: request,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getRequests = async (status, location) => {
    try {
        let query = {};

        if (status) {
            query.status = parseInt(status); 
        }
        if (location) {
            query.location = location; 
        }

        const requests = await Request.find(query)
            .populate({  path : 'location', model: 'Location', })
            .populate({  path : 'creator', model: 'User', })
            .populate({  path : 'provider', model: 'User', });
        
        return {
            status: true,
            payload: requests,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const fulfillRequest = async (id, quantity, provider) => {
    try {
        let createdRequest = null;
        const request = await Request.findOne({ _id: id });

        if (!request) {
            return {
                status: false,
                payload: "Request not found",
            };
        }
        else if (request.status !== 1) {
            return {
                status: false,
                payload: "Request is not active",
            };
        }

        if (request.productQuantity > quantity) {
            createdRequest = (
                await addRequest({
                    location: request.location,
                    productName: request.productName,
                    creator: request.creator,
                    productQuantity: request.productQuantity - quantity,
                    createdOn: request.createdOn,
                })
            ).payload;
        }

        console.log(request.location, {
            name: request.productName,
            pending: quantity,
        })
        const location = (
            await updateLocationStock(request.location, {
                name: request.productName,
                pending: quantity,
            })
        ).payload;

        request.productQuantity = quantity;
        request.provider = provider;
        request.status = 2;
        await request.save();
        
        return {
            status: true,
            payload: {
                fulfilled: request,
                created: createdRequest,
                location: location,
            },
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const resolveRequest = async (id) => {
    try {
        const request = await Request.findOne({ _id: id });

        if (!request) {
            return {
                status: false,
                payload: "Request not found",
            };
        }
        else if (request.status !== 2) {
            return {
                status: false,
                payload: "Request is not pending",
            };
        }

        const location = (
            await updateLocationStock(request.location, {
                name: request.productName,
                available: request.productQuantity,
                pending: -request.productQuantity,
            })
        ).payload;

        request.status = 3;
        await request.save();
        
        return {
            status: true,
            payload: {
                resolved: request,
                location: location,
            },
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    addRequest,
    getRequests,
    fulfillRequest,
    resolveRequest,
}
