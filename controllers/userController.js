const User = require("../models/user");
const JWT = require("jsonwebtoken");

const addUser = async (userObj) => {
    try {
        const checkUsers = await User.find({ $or: [
            { email: userObj.email },
        ] });

        if (checkUsers?.length !== 0) {
            return {
                status: false,
                payload: "Email should be unique",
            }
        }

        const user = new User(userObj);
        await user.save();
        
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const getUsers = async (location) => {
    try {
        let searchQuery = {};

        if (location) {
            searchQuery.location = location;
        }

        const users = await User.find(searchQuery)
            .populate({  path : 'location', model: 'Location', });
        
        return {
            status: true,
            payload: users,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

const toogleBlockUser = async (id) => {
    try {
        const user = await User.findOne({ _id: id });

        if (!user) {
            return {
                status: false,
                payload: "User not found",
            };
        }
        else if (user.role === 1) {
            return {
                status: false,
                payload: "This user is an admin",
            };
        }

        user.blocked = !user.blocked;
        user.save();
        
        return {
            status: true,
            payload: user,
        };
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = {
    getUsers,
    addUser,
    toogleBlockUser,
}
