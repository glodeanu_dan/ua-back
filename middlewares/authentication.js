const User = require("../models/user");
const JWT = require("jsonwebtoken");
const { JWT_KEY, } = require("../utils/constants");

const authoriseUser = roles => async(req, res, next) => {
    if (roles === null || roles === undefined) {
        res.status(500).json({ message: "Internal server error" })
        next();
    }
    else if (roles.length === 0) {
        next();
    }
    else if (req.headers.token) {
        const token = req.headers.token;
        JWT.verify(token, JWT_KEY, async(err, payload) => {
            if (err) {
                console.log('err', err)
                res.status(403).json({ message: "Invalid token, please login again" })
            } 
            else {
                try {
                    const userData = await User.findOne({ _id: payload.id })
                        .populate({  path : 'location', model: 'Location', });

                    req.userData = userData;

                    if (userData.blocked) {
                        res.status(403).json({ message: "Your account is blocked"})
                    } 
                    else if (!roles.includes(0) && !roles.includes(userData.role)) {
                        res.status(403).json({ message: "Your don't have rights for this operation"})
                    } 
                    else {
                        next();
                    }
                } 
                catch (error) {
                    res.sendStatus(500);
                }
            }
        })
    } 
    else {
        res.status(401).json({ message: "Please provide identity token" })
    }
}

module.exports = {
    authoriseUser,
}