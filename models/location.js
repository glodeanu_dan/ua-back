const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stockSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    available: {
        type: Number,
        default: 0,
    },
    pending: {
        type: Number,
        default: 0,
    },
})

const locationSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
        unique: true,
    },
    description: {
        type: String,
        default: '',
    },
    stock: {
        type: [stockSchema],
        default: [],
    },
});

const Location = mongoose.model('Location', locationSchema);

module.exports = Location;
