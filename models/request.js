const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const requestSchema = new Schema({
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    provider: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        default: null,
    },
    productName: {
        type: String,
        required: true,
    },
    productQuantity: {
        type: Number,
        required: true,
    },
    status: {
        type: Number,
        default: 1,
    },
    createdOn: {
        type: Date,
        required: true,
    }
});

const Request = mongoose.model('Request', requestSchema);

module.exports = Request;
