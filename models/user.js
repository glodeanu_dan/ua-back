const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location',
        default: null,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        default: '',
    }, 
    phone: {
        type: String,
    },
    role: {
        type: Number,
        required: true,
    },
    blocked: {
        type: Boolean,
        default: false,
    },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
