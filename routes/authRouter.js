const express = require('express');
const axios = require('axios');
const JWT = require("jsonwebtoken");
var router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  JWT_KEY,
  SESSION_TIME,
} = require("../utils/constants");
const { sendResponse, } = require("../utils/utils");
const {
  checkUserCredentials,
  checkFacebookUserExists,
} = require("../controllers/authController");
const {
  addUser,
} = require("../controllers/userController");
const {
  authoriseUser,
} = require('../middlewares/authentication');

const postLoginSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

router.post("/login", validator.body(postLoginSchema), async (req, res) => {
  try {
    var user = await checkUserCredentials(req.body.email, req.body.password);

    if (user == null) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false, status: 'Wrong email or password!' });
      return;
    }
    else if (user.blocked) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      res.json({ success: false, status: 'This user is blocked!' });
      return;
    }
    else {
      const token = JWT.sign({
        id: user._id,
        name: user.name
      },
        JWT_KEY, {
        expiresIn: SESSION_TIME[user.role]
      })

      res.status(200).json({
        _id: user._id,
        location: user.location,
        name: user.name,
        email: user.email,
        phone: user.phone,
        role: user.role,
        blocked: user.blocked,
        token,
      });
    }
  }
  catch (err) {
    return res.statusCode(500);
  }
})

router.post("/facebook-login", async (req, res) => {
  try {
    const facebookTokenValidationRes = await axios.get(
      `https://graph.facebook.com/me?access_token=${req.body.accessToken}`
    );

    if (facebookTokenValidationRes.data.id === req.body.id) {
      var user = await checkFacebookUserExists(req.body.email);
      
      if (!user) {
        user = await addUser({
          name: req.body.name,
          email: req.body.email,
          password: "",
          phone: "",
          role: 3,
        });
      }
     
      const token = JWT.sign({
        id: user._id,
        name: user.name
      },
        JWT_KEY, {
        expiresIn: SESSION_TIME[user.role]
      })

      res.status(200).json({
        _id: user._id,
        location: user.location,
        name: user.name,
        email: user.email,
        phone: user.phone,
        role: user.role,
        blocked: user.blocked,
        token,
      });
    }
    else {
      return res.sendStatus(400);
    }
  }
  catch (err) {
    console.log(err)
    return res.sendStatus(500);
  }
})

router.get('/user_data', authoriseUser([0]), async (req, res, next) => {
  try {
    return sendResponse(res, {
      status: true,
      payload: req.userData,
    });
  }
  catch (err) {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;