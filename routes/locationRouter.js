const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  getLocations,
  getLocation,
  addLocation,
  updateLocationStock,
} = require('../controllers/locationController')
const {
  authoriseUser,
} = require('../middlewares/authentication');

const { sendResponse } = require('../utils/utils')

router.get('/', authoriseUser([]), async (req, res, next) => {
  try {
    const result = await getLocations();
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.get('/:id', authoriseUser([]), async (req, res, next) => {
  try {
    const result = await getLocation(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const stockSchema = Joi.object({
  name: Joi.string().required(),
  available: Joi.number(),
  pending: Joi.number(),
});

const postLocationSchema = Joi.object({
  name: Joi.string().required(),
  address: Joi.string().required(),
  description: Joi.string(),
  stock: Joi.array().items(stockSchema),
});

router.post('/', authoriseUser([1]), validator.body(postLocationSchema), async (req, res, next) => {
  try {
    const result = await addLocation(req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.put('/product/:id', authoriseUser([1, 2]), validator.body(stockSchema), async (req, res, next) => {
  try {
    const result = await updateLocationStock(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putStockSchema = Joi.object({
  name: Joi.string().required(),
  available: Joi.number(),
  pending: Joi.number(),
});

router.put('/stock/:id', authoriseUser([1, 2]), validator.body(putStockSchema), async (req, res, next) => {
  try {
    const result = await updateLocationStock(req.params.id, req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
