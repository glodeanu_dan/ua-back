const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  addRequest,
  getRequests,
  fulfillRequest,
  resolveRequest,
} = require('../controllers/requestController')
const {
  authoriseUser,
} = require('../middlewares/authentication');

const { sendResponse } = require('../utils/utils')

const getRequestSchema = Joi.object({
  status: Joi.string().regex(/[1-3]/),
  location: Joi.string(),
});

router.get('/', authoriseUser([]), validator.query(getRequestSchema), async (req, res, next) => {
  try {
    const result = await getRequests(req.query.status, req.query.location);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const putFulfillSchema = Joi.object({
  quantity: Joi.number().required(),
});

router.put('/fulfill/:id', authoriseUser([1, 3]), validator.body(putFulfillSchema), async (req, res, next) => {
  try {
    const result = await fulfillRequest(req.params.id, req.body.quantity, req.userData._id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postRequestSchema = Joi.object({
  location: Joi.string().required(),
  productName: Joi.string().required(),
  productQuantity: Joi.number().required(),
});

router.post('/', authoriseUser([1, 2]), validator.body(postRequestSchema), async (req, res, next) => {
  try {
    const result = await addRequest({
      ...req.body,
      creator: req.userData._id,
      createdOn: new Date(),
    });

    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.put('/resolve/:id', authoriseUser([1, 2]), async (req, res, next) => {
  try {
    const result = await resolveRequest(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
