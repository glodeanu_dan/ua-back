const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});
const {
  getUsers,
  addUser,
  toogleBlockUser,
} = require('../controllers/userController')
const {
  authoriseUser,
} = require('../middlewares/authentication');

const { sendResponse } = require('../utils/utils')

const getUsersSchema = Joi.object({
  location: Joi.string(),
});

router.get(
  '/',
  authoriseUser([1, 2]),
  validator.query(getUsersSchema),
  async (req, res, next) => {
  try {
    const result = await getUsers(req.query.location);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

const postUserSchema = Joi.object({
  location: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
  phone: Joi.string().required(),
  role: Joi.number().required().min(2).max(3),
});

router.post('/', authoriseUser([1]), validator.body(postUserSchema), async (req, res, next) => {
  try {
    const result = await addUser(req.body);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

router.put('/toogle_block/:id', authoriseUser([1]), async (req, res, next) => {
  try {
    const result = await toogleBlockUser(req.params.id);
    return sendResponse(res, result);
  }
  catch {
    console.log(err);
    return res.sendStatus(500);
  }
});

module.exports = router;
