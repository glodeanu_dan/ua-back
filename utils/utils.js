const sendResponse = (res, result) => {
    if (!result) {
        return res.sendStatus(500);
    }
    else if (!result.status) {
        return res.status(400).json(result.payload);
    }
    else {
        return res.status(200).json(result.payload);
    }
}

module.exports = {
    sendResponse,
}